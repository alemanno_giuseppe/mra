package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private int planetX=0;
	private int planetY=0;	
	private int roverX=0;
	private int roverY=0;
	private char direction = 'N';
	private String obstacles;
	
	private ArrayList <String> planetObstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, ArrayList <String> planetObstacles) throws MarsRoverException {
		if(planetX<1||planetY<1)
			throw new MarsRoverException();
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles = planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		for(int i=0;i<planetObstacles.size();i++) {
			String s = planetObstacles.get(i);
			if(Character.getNumericValue(s.charAt(1))==x && Character.getNumericValue(s.charAt(3))==y) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for (int i = 0; i < commandString.length(); i++){
		    char c = commandString.charAt(i);  
			singleComand(c);
				
		}
		if(obstacles!=null)
			return "("+roverX+","+roverY+","+direction+")"+obstacles;
		return "("+roverX+","+roverY+","+direction+")";
	}

	private void singleComand(char commandString) throws MarsRoverException {
		if(commandString=='r') {
			direction = 'E';
		} else if (commandString=='l') {
			direction = 'W';
		} else if (commandString=='f') {
			movingForward();		
		} else if (commandString=='b') {
			movingBackward();			
		}
	}

	private void movingForward() throws MarsRoverException {
		if (direction == 'N') {
			setRoverY(roverY+1);
		}else if (direction == 'S') {
			setRoverY(roverY-1);
		}else if (direction == 'W') {
			setRoverX(roverX-1);
		}else if (direction == 'E') {
			setRoverX(roverX+1);
		}
	}

	private void movingBackward() throws MarsRoverException {
		if (direction == 'N') {
			setRoverY(roverY-1);
		}else if (direction == 'S') {
			setRoverY(roverY+1);
		}else if (direction == 'W') {
			setRoverX(roverX+1);
		}else if (direction == 'E') {
			setRoverX(roverX-1);
		}
	}

	public void setRoverX(int x) throws MarsRoverException {
		if (planetContainsObstacleAt(x,roverY)) {
			obstacles = "("+x+","+roverY+")";
			return;
		}
		if (x>=planetX) {
			this.roverX=0;
		} else if (x<0) {
			this.roverX=planetX-1;
		} else {
			this.roverX=x;
		}
	}

	public void setRoverY(int y) throws MarsRoverException {
		if (planetContainsObstacleAt(roverX,y)) {
			obstacles = "("+roverX+","+y+")";
			return;
		}
		if (y>=planetY) {
			this.roverY=0;
		} else if (y<0) {
			this.roverY=planetY-1;
		} else {
			this.roverY=y;
		}
	}

	public void setDirection(char c) throws MarsRoverException {
		if (c!='S' && c!='N' && c!='E' && c!='W')
			throw new MarsRoverException();
		this.direction = c;
	}

	public int getRoverX() {
		return roverX;
	}

	public int getRoverY() {
		return roverY;
	}

}
