package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

public class MarsRoverTest {
	ArrayList <String> planetObstacles;
	MarsRover marsRover;
	
	@Test (expected = MarsRoverException.class)
	public void testPlanetCWithWrongCoordinateX() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(0,10,planetObstacles);
	}
	
	@Test (expected = MarsRoverException.class)
	public void testPlanetCWithWrongCoordinateY() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,0,planetObstacles);
	}
	
	@Test
	public void testPlanetContainsObstacleAtCordinatesZeroOne() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,10,planetObstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(0, 1));
	}
	
	@Test
	public void testPlanetContainsObstacleAtCordinatesTwoTwo() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,10,planetObstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(2, 2));
	}
	
	@Test
	public void testPlanetContainsObstaclesAtCordinatesZeroOneAndTwoTwo() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,10,planetObstacles);
		
		assertTrue("First obstacle",marsRover.planetContainsObstacleAt(2, 2));		
		assertTrue("Second obstacle",marsRover.planetContainsObstacleAt(2, 2));
	}
	
	@Test
	public void testRoverLandingWithNoComand() throws Exception {
		planetObstacles = new ArrayList<>();
		marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,N)",marsRover.executeCommand(""));
	}
	
	@Test
	public void testRoverTurningLeft() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,W)",marsRover.executeCommand("l"));
	}
	
	@Test
	public void testRoverMovingForwardToTheNorth() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(7);
		marsRover.setRoverY(6);
		assertEquals("(7,7,N)",marsRover.executeCommand("f"));
	}
	
	@Test
	public void testRoverMovingForwardToTheSouth() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(7);
		marsRover.setRoverY(6);
		marsRover.setDirection('S');
		assertEquals("(7,5,S)",marsRover.executeCommand("f"));
	}
	
	@Test
	public void testRoverMovingForwardToTheWest() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(7);
		marsRover.setRoverY(6);
		marsRover.setDirection('W');
		assertEquals("(6,6,W)",marsRover.executeCommand("f"));
	}
	
	@Test
	public void testRoverMovingForwardToTheEast() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(7);
		marsRover.setRoverY(6);
		marsRover.setDirection('E');
		assertEquals("(8,6,E)",marsRover.executeCommand("f"));
	}
	
	@Test 
	public void testSetRoverXOutThePlanetLimit() throws Exception {
		planetObstacles = new ArrayList<>();
		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(7);
		marsRover.setRoverX(10);
		assertEquals(0,marsRover.getRoverX());
	}
	
	@Test 
	public void testSetRoverYOutThePlanetLimit() throws Exception {
		planetObstacles = new ArrayList<>();
		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverY(7);
		marsRover.setRoverY(10);
		assertEquals(0,marsRover.getRoverY());
	}
	
	@Test 
	public void testSetRoverXInThePlanetLimit() throws Exception {
		planetObstacles = new ArrayList<>();
		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(9);
		assertEquals(9,marsRover.getRoverX());
	}
	
	@Test 
	public void testSetRoverYInThePlanetLimit() throws Exception {
		planetObstacles = new ArrayList<>();
		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverY(7);
		marsRover.setRoverY(9);
		assertEquals(9,marsRover.getRoverY());
	}
	
	@Test
	public void testRoverMovingBackwardToTheEast() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(5);
		marsRover.setRoverY(8);
		marsRover.setDirection('E');
		assertEquals("(4,8,E)",marsRover.executeCommand("b"));
	}
	
	@Test
	public void testRoverMovingBackwardToTheWest() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(5);
		marsRover.setRoverY(8);
		marsRover.setDirection('W');
		assertEquals("(6,8,W)",marsRover.executeCommand("b"));
	}
	
	@Test
	public void testRoverMovingBackwardToTheSouth() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(5);
		marsRover.setRoverY(8);
		marsRover.setDirection('S');
		assertEquals("(5,9,S)",marsRover.executeCommand("b"));
	}
	
	@Test
	public void testRoverMovingBackwardToTheNorth() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		marsRover.setRoverX(5);
		marsRover.setRoverY(8);
		assertEquals("(5,7,N)",marsRover.executeCommand("b"));
	}
	
	@Test
	public void testRoverMovingCombinedComands() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		assertEquals("(2,2,E)",marsRover.executeCommand("ffrff"));
	}
	
	@Test
	public void testRoverMovingBackwardToTheNorthGoesInTheOppositeEdge() throws Exception {
		planetObstacles = new ArrayList<>();

		marsRover = new MarsRover(10,10,planetObstacles);
		assertEquals("(0,9,N)",marsRover.executeCommand("b"));
	}
	
	
	@Test
	public void testRoverMovingThroughSingleObstacle() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		marsRover = new MarsRover(10,10,planetObstacles);
		assertEquals("(1,2,E)(2,2)",marsRover.executeCommand("ffrfff"));
	}
	@Ignore
	@Test
	public void testRoverMovingThroughMultipleObstacle() throws Exception {
		planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		marsRover = new MarsRover(10,10,planetObstacles);
		assertEquals("(1,1,E)(2,2)(2,1)",marsRover.executeCommand("ffrfffrflf"));
	}
	
	

}
